#!/bin/sh

if [ -z $AOC_SESSION_ID ]; then
    echo "Missing env AOC_SESSION_ID"
    exit 1
fi

if [ -z $1 ]; then
    echo "Missing day number"
    exit 1
fi

DAY=$1
COOKIE="session=$AOC_SESSION_ID"

curl --cookie "$COOKIE" https://adventofcode.com/2020/day/$DAY/input > day$DAY/input.txt
