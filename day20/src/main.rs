use std::{collections::HashMap, convert::TryInto, fs::File, io::{BufRead, BufReader}, ops::Index, rc::Rc, str::FromStr};

type Position = (i64, i64);

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum Direction {
    East,
    North,
    West,
    South,
}

impl Direction {
    fn all() -> [Self; 4] {
        [
            Self::East,
            Self::North,
            Self::West,
            Self::South,
        ]
    }

    fn as_value(&self) -> usize {
        match self {
            Self::East => 0,
            Self::North => 1,
            Self::West => 2,
            Self::South => 3,
        }
    }

    fn from_value(v: usize) -> Self {
        match v {
            0 => Self::East,
            1 => Self::North,
            2 => Self::West,
            3 => Self::South,
            _ => unreachable!(),
        }
    }

    fn rotate(&self, angle: usize) -> Self {
        let quarter = self.as_value();

        let new_quarter = quarter + angle;

        Self::from_value(new_quarter % 4)
    }

    fn flipped_h(&self) -> Self {
        match self {
            Self::East => Self::East,
            Self::North => Self::South,
            Self::West => Self::West,
            Self::South => Self::North,
        }
    }

    fn opposed(&self) -> Self {
        Self::from_value((self.as_value() + 2) % 4)
    }

    fn as_direction(&self) -> Position {
        match self {
            Self::East =>  (1, 0),
            Self::North => (0, 1),
            Self::West =>  (-1, 0),
            Self::South => (0, -1),
        }
    }
}

type TileId = u32;

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Hash, Debug)]
struct TileSide {
    value: u16,
}

impl TileSide {
    const LENGTH: usize = 10;

    fn from_str(s: &str) -> Self {
        Self {
            value: s.chars()
                .map(|c| match c {
                    '.' => 0,
                    '#' => 1,
                    _ => unreachable!(),
                })
                .fold(0, |value, v| (value << 1) | v),
        }
    }

    fn from_column(values: &[Self], bit_index: usize) -> Self {
        assert!(values.len() == Self::LENGTH);

        Self {
            value: values.iter()
                .fold(0, |value, v| (value << 1) | (v.value >> bit_index) & 0x1),
        }
    }

    fn flipped(&self) -> Self {
        Self {
            value: self.value.reverse_bits() >> (8 * std::mem::size_of::<u16>() - Self::LENGTH),
        }
    }

    fn to_unique(&self) -> (TileSide, bool) {
        let flipped = self.flipped();
        assert_ne!(&flipped, self);
        if &flipped < self {
            (flipped, true)
        } else {
            (self.clone(), false)
        }
    }

    fn as_value(&self) -> u16 {
        self.value
    }
}

struct Tile {
    id: TileId,
    rows: [TileSide; TileSide::LENGTH],
    sides: [TileSide; 4],
}

impl Tile {
    fn from(lines: &[String]) -> Self {
        let mut it = lines.iter();
        let id = it.next()
            .unwrap()
            .split(' ')
            .last()
            .map(|s| TileId::from_str(&s[0..s.len() - 1])
                .unwrap())
            .unwrap();

        let rows: Vec<_> = it.map(|s| TileSide::from_str(&s))
            .collect();
        let rows: [TileSide; TileSide::LENGTH] = rows.try_into().unwrap();

        Self {
            id,
            sides: [
                TileSide::from_column(&rows,0).flipped(),
                rows[0].flipped(),
                TileSide::from_column(&rows,TileSide::LENGTH - 1),
                rows[TileSide::LENGTH - 1].clone(),
            ],
            rows,
        }
    }

    fn row(&self, index: usize) -> TileSide {
        self.rows[index].clone()
    }

    fn column(&self, index: usize) -> TileSide {
        TileSide::from_column(&self.rows, TileSide::LENGTH - index - 1)
    }

    fn side(&self, direction: &Direction) -> TileSide {
        self.sides[direction.as_value()].clone()
    }
}

type IndexedSides = HashMap<TileSide, Vec<(Rc<Tile>, Direction, bool)>>;

struct PlacedTile {
    tile: Rc<Tile>,
    flipped: bool,
    angle: usize,
}

impl PlacedTile {
    fn from(tile: Rc<Tile>, angle: usize, flipped: bool) -> Self {
        assert!(angle < 4);
        Self {
            tile,
            flipped,
            angle,
        }
    }

    fn row(&self, index: usize) -> TileSide {
        let rev_index = TileSide::LENGTH - index - 1;
        match self.angle {
            0 => self.tile.row(if self.flipped { rev_index } else { index }),
            2 => self.tile.row(if self.flipped { index } else { rev_index }).flipped(),
            1 => {
                let side = self.tile.column(rev_index);
                if self.flipped { side.flipped() } else { side }
            },
            3 => {
                let side = self.tile.column(index);
                if self.flipped { side } else { side.flipped() }
            },
            _ => unreachable!(),
        }
    }

    fn side(&self, direction: &Direction) -> TileSide {
        let direction = direction.rotate(4 - self.angle);
        let direction = if self.flipped { direction.flipped_h() } else { direction };
        let side = self.tile.side(&direction);
        if self.flipped { side.flipped() } else { side }
    }
}

struct TilePlacer<'a> {
    next: Option<PlacedTile>,
    indexed_sides: &'a IndexedSides,
    direction: Direction,
}

impl<'a> TilePlacer<'a> {
    fn new(first: PlacedTile, indexed_sides: &'a IndexedSides, direction: Direction) -> Self {
        Self {
            next: Some(first),
            indexed_sides,
            direction,
        }
    }
}

impl<'a> Iterator for TilePlacer<'a> {
    type Item = PlacedTile;

    fn next(&mut self) -> Option<PlacedTile> {
        let next = self.next.take();
        if let Some(next) = &next {
            let (key, flipped) = next.side(&self.direction).to_unique();
            if let Some((next_tile, next_direction, next_flipped)) = self.indexed_sides.get(&key)
                .unwrap()
                .iter()
                .filter(|(tile, _, _)| tile.id != next.tile.id)
                .next() {

                let must_flip = !flipped ^ next_flipped;
                let next_direction = if must_flip { next_direction.flipped_h() } else { next_direction.clone() };
                let next_direction_target = self.direction.opposed();

                self.next = Some(PlacedTile::from(Rc::clone(next_tile), (4 + next_direction_target.as_value() - next_direction.as_value()) % 4, must_flip));
            }
        }
        next
    }
}

struct Image {
    data: Vec<Vec<bool>>,
}

impl Image {
    fn new(width: usize, height: usize) -> Self {
        Self {
            data: std::iter::repeat_with(|| std::iter::repeat(false)
                    .take(width)
                    .collect())
                .take(height)
                .collect(),
        }
    }

    fn width(&self) -> usize {
        self.data[0].len()
    }

    fn height(&self) -> usize {
        self.data.len()
    }

    fn set(&mut self, x: usize, y: usize, value: bool) {
        self.data[y][x] = value;
    }

    fn get(&self, x: usize, y: usize) -> bool {
        self.data[y][x]
    }

    fn count(&self) -> usize {
        self.data.iter()
            .map(|row| row.iter()
                .filter(|&&v| v)
                .count())
            .sum()
    }

    fn pattern_fits_at(&self, x: usize, y: usize, pattern: &Image) -> bool {
        x + pattern.width() <= self.width() &&
        y + pattern.height() <= self.height() &&
        self.data.iter()
            .skip(y)
            .take(pattern.height())
            .map(|row| row.iter().skip(x).take(pattern.width()))
            .flatten()
            .zip(pattern.data.iter()
                .map(|row| row.iter())
                .flatten())
            .all(|(v, p)| !p || *p && *v)
    }

    fn remove_pattern(&mut self, x: usize, y: usize, pattern: &Image) {
        assert!(x + pattern.width() <= self.width());
        assert!(y + pattern.height() <= self.height());
        self.data.iter_mut()
            .skip(y)
            .take(pattern.height())
            .map(|row| row.iter_mut().skip(x).take(pattern.width()))
            .flatten()
            .zip(pattern.data.iter()
                .map(|row| row.iter())
                .flatten())
            .for_each(|(v, p)| *v = *v && !p);
    }

    fn remove_all_pattern(&mut self, pattern: &Image) {
        for y in 0..=(self.height() - pattern.height()) {
            for x in 0..=(self.width() - pattern.width()) {
                if self.pattern_fits_at(x, y, pattern) {
                    self.remove_pattern(x, y, pattern);
                }
            }
        }
    }
}

fn place_top_left_corner(corner: Rc<Tile>, indexed_sides: &IndexedSides) -> PlacedTile {
    let directions = Direction::all();
    let mut orphan_sides = directions.iter()
        .filter(|&d| {
            let side = corner.side(d).to_unique().0;
            indexed_sides.get(&side).unwrap().len() == 1
        });

    let rotate_to_west = match orphan_sides.next().unwrap() {
        Direction::East => match orphan_sides.next().unwrap() {
            Direction::North => Direction::East,
            Direction::South => Direction::South,
            _ => unreachable!(),
        },
        d => *d,
    };

    PlacedTile::from(corner, (4 + Direction::West.as_value() - rotate_to_west.flipped_h().as_value()) % 4, true)
}

fn create_sea_monster() -> Image {
    let mut sea_monster = Image::new(20, 3);
    sea_monster.set(18, 0, true);
    sea_monster.set(0, 1, true);
    sea_monster.set(5, 1, true);
    sea_monster.set(6, 1, true);
    sea_monster.set(11, 1, true);
    sea_monster.set(12, 1, true);
    sea_monster.set(17, 1, true);
    sea_monster.set(18, 1, true);
    sea_monster.set(19, 1, true);
    sea_monster.set(1, 2, true);
    sea_monster.set(4, 2, true);
    sea_monster.set(7, 2, true);
    sea_monster.set(10, 2, true);
    sea_monster.set(13, 2, true);
    sea_monster.set(16, 2, true);
    sea_monster
}

fn print_image(image: &Image) {
    for y in 0..image.height() {
        for x in 0..image.width() {
            print!("{}", if image.get(x, y) { "#" } else { "." });
        }
        println!("");
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day20/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let tiles_store: HashMap<TileId, Rc<Tile>> = lines
        .split(|line| line.is_empty())
        .filter(|lines| lines.len() > 1)
        .map(|tile_as_lines| {
            let tile = Tile::from(tile_as_lines);
            (tile.id, Rc::new(tile))
        })
        .collect();

    let directions = Direction::all();
    let indexed_sides: IndexedSides = tiles_store.iter()
        .map(|(_, tile)| directions.iter()
            .map(move |d| {
                let (side, flipped) = tile.side(&d).to_unique();
                (side, (tile, d.clone(), flipped))
            }))
        .flatten()
        .fold(IndexedSides::new(), |mut map, (side, (tile, direction, flipped))| {
            let tiles = map.entry(side).or_default();
            tiles.push((Rc::clone(tile), direction, flipped));
            assert!(tiles.len() <= 2);
            map
        });

    let corners: Vec<_> = indexed_sides.iter()
        .filter(|(_side, tiles)| tiles.len() == 1)
        .fold(HashMap::<TileId, usize>::new(), |mut map, (_, tiles)| {
            let (tile, _, _) = &tiles[0];
            let count = map.entry(tile.id).or_default();
            *count += 1;
            map
        })
        .into_iter()
        .filter(|(_, count)| *count == 2)
        .map(|(id, _)| id)
        .collect();

    let corners_product: u64 = corners.iter()
        .map(|&id| id as u64)
        .product();

    println!("{}", corners_product);

    let mut corners = corners;
    corners.sort();

    let top_left_corner = place_top_left_corner(Rc::clone(tiles_store.get(corners.iter().skip(1).next().unwrap()).unwrap()), &indexed_sides);

    let placed_matrix: Vec<Vec<PlacedTile>> = TilePlacer::new(top_left_corner, &indexed_sides, Direction::South)
        .map(|placed| TilePlacer::new(placed, &indexed_sides, Direction::East)
            .collect())
        .collect();

    //println!("{:?}", placed_matrix.iter().map(|row| row.len()).collect::<Vec<_>>());
    assert_eq!(placed_matrix.iter().map(|row| row.len()).sum::<usize>(), tiles_store.len());
    assert_eq!(placed_matrix.len(), placed_matrix[0].len());

    let tile_size = TileSide::LENGTH - 2;
    let image_size = placed_matrix.len() * tile_size;

    let mut image = Image::new(image_size, image_size);

    for (tile_y, row) in placed_matrix.iter().enumerate() {
        for (tile_x, tile) in row.iter().enumerate() {
            //print!("{} ", tile.tile.id);
            for y in 1..(tile_size + 1) {
                let row = tile.row(y);
                for x in 1..(tile_size + 1) {
                    image.set(tile_x * tile_size + x - 1, tile_y * tile_size + y - 1, ((row.as_value() >> ( tile_size + 1 - x)) & 0x1) == 0x1);
                }
            }
        }
        //println!();
    }

    let initial_count = image.count();

    //print_image(&image);
    
    let sea_monster = create_sea_monster();
    //println!();
    //print_image(&sea_monster);
    image.remove_all_pattern(&sea_monster);

    assert_ne!(image.count(), initial_count);

    //println!();
    //print_image(&image);

    println!("{}", image.count());
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_TILE: &str = r#"Tile 1163:
.###..##..
.#.....#.#
#...###..#
.....#....
.........#
..#...#..#
..#..##.##
.##..###.#
.........#
##.##..#.#"#;

    fn make_test_tile() -> Tile {
        let lines: Vec<String> = TEST_TILE.split('\n')
            .map(|s| String::from(s))
            .collect();
        Tile::from(&lines)
    }

    #[test]
    fn test_direction() {
        let d = Direction::North;
        assert_eq!(d.as_value(), 1);
        assert_eq!(d.rotate(3), Direction::East);
        assert_eq!(d.opposed(), Direction::South);
        assert_eq!(d.flipped_h(), Direction::South);

        let d = Direction::West;
        assert_eq!(d.as_value(), 2);
        assert_eq!(d.rotate(3), Direction::North);
        assert_eq!(d.opposed(), Direction::East);
        assert_eq!(d.flipped_h(), Direction::West);
    }

    #[test]
    fn test_tile_side() {
        let side = TileSide::from_str(".#.#..####");
        assert_eq!(side.as_value(), 0x14F);
        assert_eq!(side.flipped().as_value(), 0x3CA);
        assert_eq!(side.flipped().flipped(), side);
    }

    #[test]
    fn test_tile() {
        let tile = make_test_tile();

        assert_eq!(tile.id, 1163);
        assert_eq!(tile.side(&Direction::East).as_value(),  0x3F6);
        assert_eq!(tile.side(&Direction::North).as_value(), 0x0CE);
        assert_eq!(tile.side(&Direction::West).as_value(),  0x081);
        assert_eq!(tile.side(&Direction::South).as_value(), 0x365);

        assert_eq!(tile.row(1).as_value(), 0x105);
        assert_eq!(tile.row(2).as_value(), 0x239);
        assert_eq!(tile.row(3).as_value(), 0x010);
        assert_eq!(tile.row(4).as_value(), 0x001);
        assert_eq!(tile.row(5).as_value(), 0x089);
        assert_eq!(tile.row(6).as_value(), 0x09B);
        assert_eq!(tile.row(7).as_value(), 0x19D);
        assert_eq!(tile.row(8).as_value(), 0x001);

        assert_eq!(tile.column(0).as_value(), 0x081);
        assert_eq!(tile.column(1).as_value(), 0x305);
    }

    #[test]
    fn test_placed_tile() {
        let tile = Rc::new(make_test_tile());

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 0, true);
        assert_eq!(placed_tile.side(&Direction::East).as_value(),  0x1BF);
        assert_eq!(placed_tile.side(&Direction::North).as_value(), 0x29B);
        assert_eq!(placed_tile.side(&Direction::West).as_value(),  0x204);
        assert_eq!(placed_tile.side(&Direction::South).as_value(), 0x1CC);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 3, true);
        assert_eq!(placed_tile.side(&Direction::South).as_value(), 0x1BF);
        assert_eq!(placed_tile.side(&Direction::East).as_value(),  0x29B);
        assert_eq!(placed_tile.side(&Direction::North).as_value(), 0x204);
        assert_eq!(placed_tile.side(&Direction::West).as_value(),  0x1CC);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 3, false);
        assert_eq!(placed_tile.side(&Direction::South).as_value(), 0x3F6);
        assert_eq!(placed_tile.side(&Direction::East).as_value(),  0x0CE);
        assert_eq!(placed_tile.side(&Direction::North).as_value(), 0x081);
        assert_eq!(placed_tile.side(&Direction::West).as_value(),  0x365);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 2, false);
        assert_eq!(placed_tile.side(&Direction::West).as_value(),  0x3F6);
        assert_eq!(placed_tile.side(&Direction::South).as_value(), 0x0CE);
        assert_eq!(placed_tile.side(&Direction::East).as_value(),  0x081);
        assert_eq!(placed_tile.side(&Direction::North).as_value(), 0x365);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 1, true);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));

        let placed_tile = PlacedTile::from(Rc::clone(&tile), 1, false);
        assert_eq!(placed_tile.row(0), placed_tile.side(&Direction::North).flipped());
        assert_eq!(placed_tile.row(9), placed_tile.side(&Direction::South));
    }

    #[test]
    fn test_image() {
        let image = Image::new(1, 1);
        assert_eq!(image.get(0, 0), false);

        let mut image = Image::new(1, 1);
        let mut image2 = Image::new(1, 1);
        image.set(0, 0, true);
        assert_eq!(image.get(0, 0), true);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), true);
        image2.set(0, 0, true);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), true);
        image.remove_pattern(0, 0, &image2);
        assert_eq!(image.get(0, 0), false);

        let mut image = Image::new(2, 2);
        image.set(0, 0, true);
        image.set(0, 1, true);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), true);
        assert_eq!(image.pattern_fits_at(0, 1, &image2), true);
        assert_eq!(image.pattern_fits_at(1, 0, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 1, &image2), false);
        image.remove_pattern(0, 1, &image2);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), true);
        assert_eq!(image.pattern_fits_at(0, 1, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 0, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 1, &image2), false);
        image.remove_all_pattern(&image2);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), false);
        assert_eq!(image.pattern_fits_at(0, 1, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 0, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 1, &image2), false);

        let mut image = Image::new(2, 2);
        image.set(1, 0, true);
        image.set(1, 1, true);
        assert!(image.pattern_fits_at(0, 0, &image));
        let mut image2 = Image::new(1, 2);
        image2.set(0, 0, true);
        image2.set(0, 1, true);
        assert_eq!(image.pattern_fits_at(0, 0, &image2), false);
        assert_eq!(image.pattern_fits_at(0, 1, &image2), false);
        assert_eq!(image.pattern_fits_at(1, 0, &image2), true);
        assert_eq!(image.pattern_fits_at(1, 1, &image2), false);
        image.remove_all_pattern(&image2);
        assert_eq!(image.get(1, 0), false);
        assert_eq!(image.get(1, 1), false);
        assert_eq!(image.pattern_fits_at(1, 0, &image2), false);

        let mut image = Image::new(3, 3);
        image.set(0, 0, true);
        image.set(0, 1, true);
        image.set(1, 0, true);
        image.set(1, 1, true);
        image.set(2, 1, true);
        let mut image2 = Image::new(2, 3);
        image2.set(0, 0, true);
        image2.set(1, 0, true);
        image2.set(1, 1, true);
        image.remove_all_pattern(&image2);
        assert_eq!(image.count(), 2);

        let sea_monster = create_sea_monster();
        assert!(sea_monster.pattern_fits_at(0, 0, &sea_monster));
    }
}