use std::{fs::File, collections::HashSet, io::{BufRead, BufReader}};

type Position = Vec<i64>;

#[derive(PartialEq, Clone)]
struct SpaceCube {
    grid: HashSet<Position>,
    min: Position,
    max: Position,
    dimensions: usize,
}

impl SpaceCube {
    fn new(dimensions: usize) -> Self {
        Self {
            grid: HashSet::new(),
            min: pos_all(0, dimensions),
            max: pos_all(0, dimensions),
            dimensions,
        }
    }

    fn get(&self, pos: &Position) -> bool {
        self.grid.contains(pos)
    }

    fn set(&mut self, pos: Position) {
        assert!(pos.len() == self.dimensions);

        if self.grid.is_empty() {
            self.min = pos.clone();
            self.max = pos.clone();
        } else {
            for i in 0..self.dimensions {
                if pos[i] < self.min[i] { self.min[i] = pos[i]; }
                if pos[i] > self.max[i] { self.max[i] = pos[i]; }
            }
        }

        self.grid.insert(pos);
    }

    fn actives_count(&self) -> usize {
        self.grid.len()
    }
}

struct PositionIterator {
    from: Position,
    next: Option<Position>,
    to: Position,
}

impl PositionIterator {
    fn new(from: Position, to: Position) -> Self {
        Self {
            next: Some(from.clone()),
            from,
            to,
        }
    }

    fn compute_next(&self, current: &Position) -> Option<Position> {
        if current == &self.to {
            None
        } else {
            let mut next = current.clone();
            for ((v, to), from) in next.iter_mut().zip(self.to.iter()).zip(self.from.iter()).rev() {
                if v != to {
                    *v += 1;
                    break;
                }

                *v = *from;
            }

            Some(next)
        }
    }
}

impl Iterator for PositionIterator {
    type Item = Position;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.next.take() {
            self.next = self.compute_next(&next);
            Some(next)
        } else {
            None
        }
    }
}

fn pos_all(v: i64, dimensions: usize) -> Position {
    std::iter::repeat(v).take(dimensions).collect()
}

fn add(a: &Position, b: &Position) -> Position {
    assert!(a.len() == b.len());
    a.iter()
        .zip(b.iter())
        .map(|(a, b)| a + b)
        .collect()
}

fn displaced(a: &Position, v: i64) -> Position {
    add(a, &pos_all(v, a.len()))
}

fn get_active_neighbors(states: &SpaceCube, pos: &Position) -> usize {
    assert!(states.dimensions == pos.len());

    PositionIterator::new(displaced(pos, -1), displaced(pos, 1))
        .filter(|np| np != pos && states.get(np))
        .count()
}

fn get_next_state(states: &SpaceCube, pos: &Position) -> bool {
    let active_neighbors = get_active_neighbors(states, pos);

    active_neighbors == 3 || active_neighbors == 2 && states.get(pos)
}

fn compute_next_state(source: &SpaceCube) -> SpaceCube {
    let mut dest = SpaceCube::new(source.dimensions);

    for pos in PositionIterator::new(displaced(&source.min, -1), displaced(&source.max, 1)) {
        if get_next_state(source, &pos) {
            dest.set(pos);
        }
    }

    dest
}

fn run_cycles(start: &SpaceCube, n: usize) -> SpaceCube {
    std::iter::repeat(())
        .take(n)
        .fold(start.clone(), |states, _| compute_next_state(&states))
}

fn parse_2d(lines: &[String], dimensions: usize) -> SpaceCube {
    assert!(dimensions >= 2);

    lines.iter()
        .enumerate()
        .fold(SpaceCube::new(dimensions), |states, (y, v)| v.chars()
            .enumerate()
            .fold(states, |mut states, ( x, c)| {
                if c == '#' {
                    let pos = std::iter::once(x as i64)
                        .chain(std::iter::once(y as i64))
                        .chain(std::iter::repeat(0).take(dimensions - 2))
                        .collect();
                    
                    states.set(pos);
                }
                states
            }))
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day17/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    let states = parse_2d(&lines, 3);
    println!("{}", run_cycles(&states, 6).actives_count());

    let states = parse_2d(&lines, 4);
    println!("{}", run_cycles(&states, 6).actives_count());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_displaced() {
        assert_eq!(displaced(&pos_all(0, 2), -1), pos_all(-1, 2));
        assert_eq!(displaced(&pos_all(1, 2), 1), pos_all(2, 2));
    }

    #[test]
    fn test_iter_neighbors() {
        let neighbors: Vec<_> = PositionIterator::new(pos_all(0, 3), pos_all(0, 3)).collect();
        assert_eq!(neighbors, vec!(pos_all(0, 3)));

        let neighbors: Vec<_> = PositionIterator::new(pos_all(-1, 3), pos_all(1, 3)).collect();
        assert_eq!(neighbors.len(), 27);
        assert!(neighbors.contains(&pos_all(-1, 3)));
        assert!(neighbors.contains(&pos_all(1, 3)));
        assert!(neighbors.contains(&pos_all(0, 3)));
        assert!(neighbors.contains(&vec!(0, 1, 1)));
        assert!(neighbors.contains(&vec!(0, 1, 0)));
        assert!(neighbors.contains(&vec!(0, 1, -1)));
        assert!(neighbors.contains(&vec!(0, -1, -1)));

        let neighbors: Vec<_> = PositionIterator::new(pos_all(-1, 4), pos_all(1, 4)).collect();
        assert_eq!(neighbors.len(), 3 * 3 * 3 * 3);
    }

    #[test]
    fn test_min_max() {
        let mut states = SpaceCube::new(3);
        states.set(vec!(0, 1, 0));
        states.set(vec!(1, 1, 0));
        states.set(vec!(1, 0, 0));
        assert_eq!(&states.min, &vec!(0, 0, 0));
        assert_eq!(&states.max, &vec!(1, 1, 0));
    }

    #[test]
    fn test_actives_count() {
        let mut states = SpaceCube::new(2);
        states.set(vec!(0, 1));
        states.set(vec!(1, 1));
        states.set(vec!(1, 0));
        assert_eq!(states.actives_count(), 3);
    }

    #[test]
    fn test_get_active_neighbors() {
        let mut states = SpaceCube::new(3);
        states.set(vec!(0, 1, 0));
        states.set(vec!(1, 1, 0));
        states.set(vec!(1, 0, 0));
        assert_eq!(get_active_neighbors(&states, &vec!(0, 0, 0)), 3);
    }
}