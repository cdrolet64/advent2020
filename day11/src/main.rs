use std::{fs::File, io::{BufRead, BufReader}};

#[derive(Clone, PartialEq)]
enum SeatState {
    Empty,
    Occupied,
    None,
}

impl Default for SeatState {
    fn default() -> Self {
        Self::None
    }
}

impl From<char> for SeatState {
    fn from(c: char) -> Self {
        match c {
            'L' => Self::Empty,
            '#' => Self::Occupied,
            '.' => Self::None,
            _ => unreachable!(),
        }
    }
}

#[derive(PartialEq)]
struct Matrix<T> {
    seats: Vec<T>,
    width: usize,
    height: usize,
}

impl<T: From<char> + Default + std::clone::Clone> Matrix<T> {
    fn new(lines: &[String]) -> Self {
        let width = lines[0].len();
        let height = lines.len();
        Self {
            seats: lines.iter().map(|line| line.chars().map(|c| T::from(c))).flatten().collect(),
            width,
            height,
        }
    }

    fn new_with_size(width: usize, height: usize) -> Self {
        Self {
            seats: std::iter::repeat(T::default()).take(width * height).collect(),
            width,
            height,
        }
    }

    fn get(&self, x: usize, y: usize) -> &T {
        &self.seats[y * self.width + x]
    }

    fn set(&mut self, x: usize, y: usize, value: T) {
        self.seats[y * self.width + x] = value;
    }
}

type GetOccupied = fn(&Matrix<SeatState>, usize, usize) -> usize;

const DIRS: &[(i32, i32); 8] = &[
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, -1),
    (0, 1),
    (1, -1),
    (1, 0),
    (1, 1),
];

fn get_occupied_part1(states: &Matrix<SeatState>, x: usize, y: usize) -> usize {
    DIRS.iter()
        .map(|(cx, cy)| (x as i32 + *cx, y as i32 + *cy))
        .filter(|(cx, cy)| *cx >= 0 && *cx < states.width as i32 && *cy >= 0 && *cy < states.height as i32)
        .map(|(cx, cy)| states.get(cx as usize, cy as usize))
        .filter(|&seat| *seat == SeatState::Occupied)
        .count()
}

fn occupied_visible(states: &Matrix<SeatState>, x: usize, y: usize, dx: i32, dy: i32) -> bool {
    let mut cx = x as i32 + dx;
    let mut cy = y as i32 + dy;

    while cx >= 0 && cy >= 0 && cx < (states.width as i32) && cy < (states.height as i32) {
        match states.get(cx as usize, cy as usize) {
            SeatState::Occupied => return true,
            SeatState::Empty => return false,
            _ => {
                cx += dx;
                cy += dy;
            },
        }
    }

    false
}

fn get_occupied_part2(states: &Matrix<SeatState>, x: usize, y: usize) -> usize {
    DIRS.iter()
        .map(|(dx, dy)| occupied_visible(states, x, y, *dx, *dy))
        .filter(|&b| b)
        .count()
}

fn get_next_state(states: &Matrix<SeatState>, x: usize, y: usize, state: &SeatState, count: usize, occupied: GetOccupied) -> SeatState {
    if *state == SeatState::Empty && occupied(states, x, y) == 0 {
        SeatState::Occupied
    } else if *state == SeatState::Occupied && occupied(states, x, y) >= count {
        SeatState::Empty
    } else {
        state.clone()
    }
}

fn compute_next_state(source: &Matrix<SeatState>, dest: &mut Matrix<SeatState>, count: usize, occupied: GetOccupied) {
    for y in 0..source.height {
        for x in 0..source.width {
            dest.set(x, y, get_next_state(source, x, y, source.get(x, y), count, occupied));
        }
    }
}

fn count_occupied(states: &Matrix<SeatState>) -> usize {
    let mut count = 0;
    for y in 0..states.height {
        for x in 0..states.width {
            if *states.get(x, y) == SeatState::Occupied {
                count += 1;
            }
        }
    }

    count
}

fn run_until_stable(lines: &[String], count: usize, occupied: GetOccupied) -> usize {
    let mut states_a = Matrix::new(lines);
    let mut states_b = Matrix::new_with_size(states_a.width, states_a.height);

    loop {
        compute_next_state(&states_a, &mut states_b, count, occupied);
        if states_a == states_b {
            return count_occupied(&states_a);
        }
        compute_next_state(&states_b, &mut states_a, count, occupied);
        if states_a == states_b {
            return count_occupied(&states_a);
        }
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day11/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    println!("{}", run_until_stable(&lines, 4, get_occupied_part1));
    println!("{}", run_until_stable(&lines, 5, get_occupied_part2));
}
