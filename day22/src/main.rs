use std::{cmp::Ordering, collections::{HashSet, VecDeque}, fs::File, io::{BufRead, BufReader}, str::FromStr};

type Cards = VecDeque<usize>;

#[derive(PartialEq, Eq, Hash, Clone)]
struct PlayerDeck {
    name: String,
    cards: Cards,
}

impl PlayerDeck {
    fn new(name: String, cards: Cards) -> Self {
        Self {
            name,
            cards,
        }
    }

    fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    fn remaining_cards(&self) -> usize {
        self.cards.len()
    }

    fn clone_with(&self, count: usize) -> Self {
        Self {
            name: self.name.clone(),
            cards: self.cards.iter().take(count).cloned().collect(),
        }
    }

    fn draw(&mut self) -> usize {
        self.cards.pop_front().unwrap()
    }

    fn push_under(&mut self, card: usize) {
        self.cards.push_back(card);
    }

    fn score(&self) -> usize {
        self.cards.iter()
            .rev()
            .enumerate()
            .map(|(index, card)| *card * (index + 1))
            .sum()
    }
}

fn run_combat(deck_a: &mut PlayerDeck, deck_b: &mut PlayerDeck) {
    while !deck_a.is_empty() && !deck_b.is_empty() {
        let a = deck_a.draw();
        let b = deck_b.draw();

        match a.cmp(&b) {
            Ordering::Less => {
                deck_b.push_under(b);
                deck_b.push_under(a);
            },
            Ordering::Greater => {
                deck_a.push_under(a);
                deck_a.push_under(b);
            }
            Ordering::Equal => unreachable!(),
        }
    }
}

enum RecursiveGameOutcome {
    Loop,
    PlayerAWins,
    PlayerBWins,
}

fn run_recursive_combat(deck_a: &mut PlayerDeck, deck_b: &mut PlayerDeck) -> RecursiveGameOutcome {
    let mut round_history = HashSet::<(PlayerDeck, PlayerDeck)>::new();

    while !deck_a.is_empty() && !deck_b.is_empty() {
        if !round_history.insert((deck_a.clone(), deck_b.clone())) {
            return RecursiveGameOutcome::Loop;
        }

        let a = deck_a.draw();
        let b = deck_b.draw();

        let a_wins_round = if deck_a.remaining_cards() >= a && deck_b.remaining_cards() >= b {
            match run_recursive_combat(&mut deck_a.clone_with(a), &mut deck_b.clone_with(b)) {
                RecursiveGameOutcome::Loop => true,
                RecursiveGameOutcome::PlayerAWins => true,
                RecursiveGameOutcome::PlayerBWins => false,
            }
        } else {
            match a.cmp(&b) {
                Ordering::Less => {
                    false
                },
                Ordering::Greater => {
                    true
                }
                Ordering::Equal => unreachable!(),
            }
        };

        if a_wins_round {
            deck_a.push_under(a);
            deck_a.push_under(b);
        } else {
            deck_b.push_under(b);
            deck_b.push_under(a);
        }
    }

    match deck_a.is_empty() {
        true => RecursiveGameOutcome::PlayerBWins,
        false => RecursiveGameOutcome::PlayerAWins,
    }
}

fn parse_deck(lines: &[String]) -> PlayerDeck {
    let mut it = lines.iter();
    let name = it.next().map(|s| &s[0..s.len() - 1]).unwrap().into();
    let cards = it.map(|line| usize::from_str(&line).unwrap()).collect();
    PlayerDeck::new(name, cards)
}

fn parse_decks(lines: &[String]) -> Vec<PlayerDeck> {
    lines.split(|line| line.is_empty())
        .map(|lines| parse_deck(&lines))
        .collect()
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day22/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let decks = parse_decks(&lines);
    let mut it = decks.into_iter();

    let deck_a = it.next().unwrap();
    let deck_b = it.next().unwrap();

    {
        let mut deck_a = deck_a.clone();
        let mut deck_b = deck_b.clone();
        run_combat(&mut deck_a, &mut deck_b);

        let winner = if deck_a.is_empty() { deck_b } else { deck_a };
        println!("{}", winner.score());
    }

    {
        let mut deck_a = deck_a.clone();
        let mut deck_b = deck_b.clone();
        let outcome = run_recursive_combat(&mut deck_a, &mut deck_b);

        let winner = match outcome {
            RecursiveGameOutcome::Loop | RecursiveGameOutcome::PlayerAWins => deck_a,
            RecursiveGameOutcome::PlayerBWins => deck_b,
        };

        println!("{}", winner.score());
    }
}
