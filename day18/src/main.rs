use std::{str::Chars, fs::File, io::{BufRead, BufReader}};

#[derive(PartialEq, Debug)]
enum Operator {
    Addition,
    Multiplication,
}

impl Operator {
    fn compute(&self, left: i64, right: i64) -> i64 {
        match self {
            Self::Addition => left + right,
            Self::Multiplication => left * right,
        }
    }
}

#[derive(Debug)]
enum Element {
    Value(i64),
    Operator(Operator),
    Expression(Expression),
}

#[derive(Debug)]
struct Expression {
    elements: Vec<Element>,
}

impl Expression {
    fn from<'a>(chars: &mut Chars<'a>) -> Self {
        let mut elements = Vec::new();

        while let Some(c) = chars.next() {
            match c {
                '(' => elements.push(Element::Expression(Expression::from(chars))),
                ')' => break,
                '0'..='9' => {
                    let digit = c as i64 - '0' as i64;
                    if let Some(Element::Value(v)) = elements.last_mut() {
                        *v = *v * 10 + digit;
                        continue;
                    }
                    elements.push(Element::Value(digit));
                },
                '+' => elements.push(Element::Operator(Operator::Addition)),
                '*' => elements.push(Element::Operator(Operator::Multiplication)),
                ' ' => (),
                _ => unreachable!(),
            }
        }

        Self {
            elements,
        }
    }

    fn compute(self, priority: &[&[Operator]]) -> i64 {
        let Self { elements } = self;

        let mut elements: Vec<_> = elements.into_iter().map(|element|
            if let Element::Expression(expression) = element {
                Element::Value(expression.compute(priority))
            } else {
                element
            }).collect();

        for operators in priority {
            let mut i = 1;
            while i < elements.len() {
                let result = match &elements[i] {
                    Element::Operator(operator) => {
                        if operators.iter().find(|op| operator == *op).is_none() {
                            i += 2;
                            continue;
                        }

                        let left = match elements[i - 1] {
                            Element::Value(v) => v,
                            _ => unreachable!(),
                        };
                        let right = match elements[i + 1] {
                            Element::Value(v) => v,
                            _ => unreachable!(),
                        };
                        operator.compute(left, right)
                    },
                    _ => unreachable!(),
                };

                elements.splice(i - 1..i + 2, std::iter::once(Element::Value(result)));
            }
        }

        match elements.first().unwrap() {
            Element::Value(v) => *v,
            _ => unreachable!(),
        }
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day18/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    let expressions: Vec<_> = lines.iter()
        .map(|line| Expression::from(&mut line.chars()))
        .collect();
    println!("{}", expressions.into_iter().map(|expression| expression.compute(&[&[Operator::Addition, Operator::Multiplication]])).sum::<i64>());

    let expressions: Vec<_> = lines.iter()
        .map(|line| Expression::from(&mut line.chars()))
        .collect();
    println!("{}", expressions.into_iter().map(|expression| expression.compute(&[&[Operator::Addition], &[Operator::Multiplication]])).sum::<i64>());
}
