fn find_exponent(subject: usize, key: usize, p: usize) -> usize {
    let mut value = 1;
    let mut exp = 0;
    while value != key {
        value = (value * subject) % p;
        exp += 1;
    }

    exp
}

fn power_mod(subject: usize, exp: usize, p: usize) -> usize {
    (0..exp).into_iter()
        .fold(1, |value, _| (value * subject) % p)
}

fn main() {
    let p = 20201227;
    let card_public_key = 3248366;
    let door_public_key = 4738476;
    let subject = 7;

    let door = find_exponent(subject, door_public_key, p);
    println!("{}", power_mod(card_public_key, door, p));
}

#[cfg(test)]
mod tests {
    use crate::{find_exponent, power_mod};

    #[test]
    fn test_power_mod() {
        assert_eq!(power_mod(3, 2, 7), 2);
        assert_eq!(power_mod(3, 3, 7), 6);
        assert_eq!(power_mod(5, 2, 11), 3);
    }

    #[test]
    fn test_find_exp() {
        assert_eq!(find_exponent(3, 2, 7), 2);
        assert_eq!(find_exponent(3, 6, 7), 3);
        assert_eq!(find_exponent(5, 3, 11), 2);

        assert_eq!(find_exponent(7, 5764801, 20201227), 8);
        assert_eq!(find_exponent(7, 17807724, 20201227), 11);
    }
}
