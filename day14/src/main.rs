use std::{fs::File, io::{BufRead, BufReader}};
use std::str::FromStr;

type MemValue = u64;

mod part1 {
    use std::{collections::BTreeMap, str::FromStr};

    use super::MemValue;

    pub enum Instruction {
        Mask(MemValue, MemValue), // AND mask, OR mask
        Mem(MemValue, MemValue),  // Address, Value
    }

    impl FromStr for Instruction {
        type Err = ();

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let mut parts = s.split(" = ");

            let left = parts.next().unwrap();
            if left == "mask" {
                let mask_value = parts.next().unwrap();
                let (and_mask, or_mask) = mask_value.chars().fold((0u64, 0u64), |(and_mask, or_mask ), c| {
                    let (next_and, next_or) = match c {
                        'X' => (1, 0),
                        '1' => (1, 1),
                        '0' => (0, 0),
                        _ => unreachable!(),
                    };
                    ((and_mask << 1) | next_and, (or_mask << 1) | next_or)
                });

                Ok(Self::Mask(and_mask, or_mask))
            } else {
                let address = MemValue::from_str(&left[4..(left.len() - 1)]).unwrap();
                let value = MemValue::from_str(parts.next().unwrap()).unwrap();
                Ok(Self::Mem(address, value))
            }
        }
    }

    pub fn execute(instructions: &[Instruction]) -> MemValue {
        instructions.iter().fold((BTreeMap::<MemValue, MemValue>::new(), (0u64, 0u64)), |(mut memory, mask), instruction| {
            match instruction {
                Instruction::Mask(and_mask, or_mask) => (memory, (*and_mask, *or_mask)),
                Instruction::Mem(address, value) => {
                    memory.insert(*address, value & mask.0 | mask.1);
                    (memory, mask)
                },
            }
        }).0.values().sum()
    }
}

mod part2 {
    use std::{collections::HashMap, str::FromStr};

    use super::MemValue;

    pub enum Instruction {
        Mask(MemValue, MemValue), // OR mask, X mask
        Mem(MemValue, MemValue),  // Address, Value
    }

    impl FromStr for Instruction {
        type Err = ();

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let mut parts = s.split(" = ");

            let left = parts.next().unwrap();
            if left == "mask" {
                let mask_value = parts.next().unwrap();
                let (or_mask, x_mask) = mask_value.chars().fold((0u64, 0u64), |(or_mask, x_mask ), c| {
                    let (next_or, next_x) = match c {
                        'X' => (0, 1),
                        '1' => (1, 0),
                        '0' => (0, 0),
                        _ => unreachable!(),
                    };
                    ((or_mask << 1) | next_or, (x_mask << 1) | next_x)
                });

                Ok(Self::Mask(or_mask, x_mask))
            } else {
                let address = MemValue::from_str(&left[4..(left.len() - 1)]).unwrap();
                let value = MemValue::from_str(parts.next().unwrap()).unwrap();
                Ok(Self::Mem(address, value))
            }
        }
    }

    pub struct XAddressIterator {
        address: MemValue,
        x_mask: MemValue,
        done: bool,
    }

    impl XAddressIterator {
        pub fn new(address: MemValue, x_mask: MemValue) -> Self {
            let address = address & !x_mask;
            Self {
                address,
                x_mask,
                done: false,
            }
        }

        fn increment(&mut self) {
            let mut inc_next = true;
            let mut inc_index = 0u32;
            let mut rx_mask = self.x_mask;

            while inc_next {
                let tz = rx_mask.trailing_zeros();
                if tz >= 36 {
                    self.done = true;
                    return;
                }

                inc_index += tz;

                inc_next = ((self.address >> inc_index) & 1) == 1;
                if inc_next {
                    self.address &= !(1u64 << inc_index);
                } else {
                    self.address |= 1u64 << inc_index;
                }

                inc_index += 1;
                rx_mask >>= tz + 1;
            }
        }
    }

    impl Iterator for XAddressIterator {
        type Item = MemValue;

        fn next(&mut self) -> Option<Self::Item> {
            if self.done {
                return None;
            }

            let current = self.address;
            self.increment();
            Some(current)
        }
    }

    pub fn execute(instructions: &[Instruction]) -> MemValue {
        instructions.iter().fold((HashMap::<MemValue, MemValue>::new(), (0u64, 0u64)), |(memory, mask), instruction| {
            match instruction {
                Instruction::Mask(or_mask, x_mask) => (memory, (*or_mask, *x_mask)),
                Instruction::Mem(address, value) => {
                    let base_address = *address | mask.0;
                    let memory = XAddressIterator::new(base_address, mask.1).fold(memory, |mut memory, address| {
                        memory.insert(address, *value);
                        memory
                    });
                    (memory, mask)
                },
            }
        }).0.values().sum()
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day14/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    {
        use part1::*;
        let instructions: Vec<Instruction> = lines.iter().map(|line| Instruction::from_str(&line).unwrap()).collect();

        println!("{}", execute(&instructions));
    }

    {
        use part2::*;
        let instructions: Vec<Instruction> = lines.iter().map(|line| Instruction::from_str(&line).unwrap()).collect();

        println!("{}", execute(&instructions));
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    #[test]
    fn test_part1_execute() {
        use super::part1::*;

        let instructions = [
            Instruction::Mask(0xffffffffdu64, 0x000000040u64),
            Instruction::Mem(8, 11),
            Instruction::Mem(7, 101),
            Instruction::Mem(8, 0),
        ];

        assert_eq!(execute(&instructions), 165);
    }

    #[test]
    fn test_part1_parse_execute() {
        use super::part1::*;

        let lines = [
            "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
            "mem[8] = 11",
            "mem[7] = 101",
            "mem[8] = 0",
        ];
        let instructions: Vec<Instruction> = lines.iter().map(|line| Instruction::from_str(&line).unwrap()).collect();

        assert_eq!(execute(&instructions), 165);
    }

    #[test]
    fn test_part2_parse_execute() {
        use super::part2::*;

        let lines = [
            "mask = 000000000000000000000000000000X1001X",
            "mem[42] = 100",
            "mask = 00000000000000000000000000000000X0XX",
            "mem[26] = 1",
        ];
        let instructions: Vec<Instruction> = lines.iter().map(|line| Instruction::from_str(&line).unwrap()).collect();

        assert_eq!(execute(&instructions), 208);
    }

    #[test]
    fn test_part2_x_address_iterator() {
        use super::part2::XAddressIterator;

        let mut iter = XAddressIterator::new(1, 0x0);
        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), None);

        let mut iter = XAddressIterator::new(1, 0x1);
        assert_eq!(iter.next(), Some(0));
        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), None);

        let values: Vec<_> = XAddressIterator::new(0x00, 0x03).collect();
        assert_eq!(&values, &[0, 1, 2, 3]);

        let values: Vec<_> = XAddressIterator::new(0x00, 0x11).collect();
        assert_eq!(&values, &[0, 1, 16, 17]);
    }
}