use std::{collections::HashSet, fs::File, io::{BufRead, BufReader}};

type Position = Vec<i64>;

enum Direction {
    East,
    NorthEast,
    NorthWest,
    West,
    SouthWest,
    SouthEast,
}

impl Direction {
    fn all() -> Vec<Direction> {
        vec![
            Self::East,
            Self::NorthEast,
            Self::NorthWest,
            Self::West,
            Self::SouthWest,
            Self::SouthEast,
        ]
    }

    fn from<T: Iterator<Item = char>>(it: &mut T) -> Option<Self> {
        it.next().map(|c|
            match c {
                'e' => Self::East,
                'w' => Self::West,
                'n' => match it.next().unwrap() {
                    'e' => Self::NorthEast,
                    'w' => Self::NorthWest,
                    _ => unreachable!(),
                },
                's' => match it.next().unwrap() {
                    'e' => Self::SouthEast,
                    'w' => Self::SouthWest,
                    _ => unreachable!(),
                },
                _ => unreachable!(),
            })
    }

    fn as_position(&self) -> Position {
        match self {
            Self::East => vec![1, 0],
            Self::NorthEast => vec![1, 1],
            Self::NorthWest => vec![0, 1],
            Self::West => vec![-1, 0],
            Self::SouthWest => vec![-1, -1],
            Self::SouthEast => vec![0, -1],
        }
    }
}

struct ParseDirectionIterator<T: Iterator<Item = char>> {
    char_it: T,
}

impl<T: Iterator<Item = char>> ParseDirectionIterator<T> {
    fn from(char_it: T) -> Self {
        Self {
            char_it,
        }
    }
}

impl<T: Iterator<Item = char>> Iterator for ParseDirectionIterator<T> {
    type Item = Direction;

    fn next(&mut self) -> Option<Self::Item> {
        Direction::from(&mut self.char_it)
    }
}

struct PositionIterator {
    from: Position,
    next: Option<Position>,
    to: Position,
}

impl PositionIterator {
    fn new(from: Position, to: Position) -> Self {
        Self {
            next: Some(from.clone()),
            from,
            to,
        }
    }

    fn compute_next(&self, current: &Position) -> Option<Position> {
        if current == &self.to {
            None
        } else {
            let mut next = current.clone();
            for ((v, to), from) in next.iter_mut().zip(self.to.iter()).zip(self.from.iter()).rev() {
                if v != to {
                    *v += 1;
                    break;
                }

                *v = *from;
            }

            Some(next)
        }
    }
}

impl Iterator for PositionIterator {
    type Item = Position;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.next.take() {
            self.next = self.compute_next(&next);
            Some(next)
        } else {
            None
        }
    }
}

struct HexagonalMap {
    flipped: HashSet<Position>,
    min: Position,
    max: Position,
}

impl HexagonalMap {
    fn new() -> Self {
        Self {
            flipped: HashSet::new(),
            min: vec![0, 0],
            max: vec![0, 0],
        }
    }

    fn flip(&mut self, pos: &Position) {
        if self.flipped.is_empty() {
            self.min = pos.clone();
            self.max = pos.clone();
        } else {
            for i in 0..self.min.len() {
                if pos[i] < self.min[i] { self.min[i] = pos[i]; }
                if pos[i] > self.max[i] { self.max[i] = pos[i]; }
            }
        }

        if self.flipped.take(pos).is_none() {
            self.flipped.insert(pos.clone());
        }
    }

    fn is_flipped(&self, pos: &Position) -> bool {
        self.flipped.get(pos).is_some()
    }

    fn flipped_count(&self) -> usize {
        self.flipped.len()
    }
}

fn pos_all(v: i64, dimensions: usize) -> Position {
    std::iter::repeat(v).take(dimensions).collect()
}

fn add(a: &Position, b: &Position) -> Position {
    assert!(a.len() == b.len());
    a.iter()
        .zip(b.iter())
        .map(|(a, b)| a + b)
        .collect()
}

fn displaced(a: &Position, v: i64) -> Position {
    add(a, &pos_all(v, a.len()))
}

fn get_active_neighbors(states: &HexagonalMap, pos: &Position) -> usize {
    Direction::all().into_iter()
        .map(|d| add(pos, &d.as_position()))
        .filter(|np| states.is_flipped(np))
        .count()
}

fn get_next_state(states: &HexagonalMap, pos: &Position) -> bool {
    let active_neighbors = get_active_neighbors(states, pos);

    states.is_flipped(pos) && active_neighbors == 1 || active_neighbors == 2
}

fn compute_next_state(source: &HexagonalMap) -> HexagonalMap {
    let mut dest = HexagonalMap::new();

    for pos in PositionIterator::new(displaced(&source.min, -1), displaced(&source.max, 1)) {
        if get_next_state(source, &pos) {
            dest.flip(&pos);
        }
    }

    dest
}

fn compute_position<T: Iterator<Item = Direction>>(directions: T) -> Position {
    directions.map(|direction| direction.as_position())
        .fold(vec![0, 0], |pos, rel_pos| vec![pos[0] + rel_pos[0], pos[1] + rel_pos[1]])
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day24/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    let map = lines.iter()
        .map(|line| compute_position(ParseDirectionIterator::from(line.chars())))
        .fold(HexagonalMap::new(), |mut map, pos| {
            map.flip(&pos);
            map
        });
    println!("{}", map.flipped_count());

    let final_map = (0..100).into_iter()
        .fold(map, |map, _| compute_next_state(&map));
    println!("{}", final_map.flipped_count());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hexagonal_map() {
        let mut map = HexagonalMap::new();
        let origin = vec![0, 0];
        map.flip(&origin);
        assert_eq!(get_active_neighbors(&map, &origin), 0);
        assert_eq!(get_active_neighbors(&map, &vec![1, 1]), 1);
        assert_eq!(get_active_neighbors(&map, &vec![1, -1]), 0);
        map.flip(&origin);
        assert_eq!(get_active_neighbors(&map, &origin), 0);
        assert_eq!(get_active_neighbors(&map, &vec![1, 1]), 0);
        assert_eq!(get_active_neighbors(&map, &vec![1, -1]), 0);
    }
}