use std::{collections::{BTreeMap, HashMap, HashSet}, fs::File, io::{BufRead, BufReader}};

fn parse_food(line: &str) -> (HashSet<String>, HashSet<String>) {
    let mut parts = line[0..line.len() - 1].split(" (contains ");
    let ingredients: HashSet<_> = parts.next().unwrap().split(' ').map(|s| s.into()).collect();
    let allergens: HashSet<_> = parts.next().unwrap().split(", ").map(|s| s.into()).collect();
    (ingredients, allergens)
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day21/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let foods: Vec<_> = lines.iter().map(|line| parse_food(&line)).collect();
    let ingredients_per_allergens = foods.iter()
        .map(|(ingredients, allergens)| allergens.iter().map(move |allergen| (allergen, ingredients)))
        .flatten()
        .fold(HashMap::<String, HashSet<String>>::new(), |mut ingredients_per_allergens, (allergen, ingredients)| {
            ingredients_per_allergens.entry(allergen.clone())
                .and_modify(|v| *v = v.intersection(&ingredients).cloned().collect())
                .or_insert_with(|| ingredients.clone());
            ingredients_per_allergens
        });

    let all_ingredients = foods.iter()
        .map(|(ingredients, _)| ingredients)
        .fold(HashSet::new(), |ingredients, ingredients_to_add| {
            ingredients.union(&ingredients_to_add).cloned().collect()
        });

    let ingredients_possibly_with_allergens = ingredients_per_allergens.values()
        .fold(HashSet::new(), |ingredients, ingredients_to_add| {
            ingredients.union(&ingredients_to_add).cloned().collect()
        });

    let safe_ingredients: HashSet<_> = all_ingredients.difference(&ingredients_possibly_with_allergens)
        .cloned()
        .collect();

    let safe_count: usize = foods.iter()
        .map(|(ingredients, _)| ingredients.intersection(&safe_ingredients).count())
        .sum();

    println!("{}", safe_count);

    let mut ingredient_per_allergen = BTreeMap::new();
    let mut allergens_to_solve = ingredients_per_allergens.clone();

    while !allergens_to_solve.is_empty() {
        let solved: Vec<_> = allergens_to_solve.iter()
            .filter(|(_, ingredients)| ingredients.len() == 1)
            .map(|(allergen, ingredients)|
                (allergen.clone(), ingredients.iter()
                    .next()
                    .cloned()
                    .unwrap()))
            .collect();

        let solved_ingredients: HashSet<_> = solved.iter()
            .map(|p| p.1.clone())
            .collect();

        solved.into_iter().for_each(|(allergen, ingredient)| {
            allergens_to_solve.remove(&allergen);
            ingredient_per_allergen.insert(allergen, ingredient);
        });

        allergens_to_solve.iter_mut()
            .for_each(|(_, ingredients)|
                *ingredients = ingredients.difference(&solved_ingredients)
                    .cloned()
                    .collect());
    }
    println!("{}", ingredient_per_allergen.values().cloned().collect::<Vec<_>>().join(","));
}
