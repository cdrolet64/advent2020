use std::{collections::BTreeSet, fs::File, io::{BufRead, BufReader}};
use std::str::FromStr;

const MOVING_WINDOW_SIZE: usize = 25;

struct MovingWindow<'a> {
    data: &'a [u64],
    indexed: BTreeSet<u64>,
    position: usize,
}

impl<'a> MovingWindow<'a> {
    fn new(data: &'a [u64]) -> Self {
        let indexed: BTreeSet<_> = data.iter().take(MOVING_WINDOW_SIZE).cloned().collect();
        assert!(indexed.len() == MOVING_WINDOW_SIZE);

        Self {
            data,
            indexed,
            position: MOVING_WINDOW_SIZE,
        }
    }

    fn advance(&mut self) -> Option<u64> {
        let to_remove = self.data[self.position - MOVING_WINDOW_SIZE];
        let to_add = self.data[self.position];
        assert!(self.indexed.remove(&to_remove));
        assert!(self.indexed.insert(to_add));
        self.position += 1;

        let new_value = self.data[self.position];
        match self.indexed.iter().find(|&&v| {
            if new_value < v { return false; }
            self.indexed.get(&(new_value - v)).is_some()
        }) {
            Some(_) => None,
            None => Some(new_value),
        }
    }
}

fn find_breaking_number(data: &[u64]) -> u64{
    let mut window = MovingWindow::new(&data);

    loop {
        if let Some(found) = window.advance() {
            return found;
        }
    }
}

fn find_contiguous_sum(data: &[u64], target: u64) -> u64 {
    let mut first = 0;
    let mut last = 0;
    let mut sum = data[0];

    loop {
        if sum == target {
            let serie = &data[first..=last];
            return serie.iter().min().unwrap() + serie.iter().max().unwrap();
        }

        if sum < target {
            last += 1;
            sum += data[last];
        } else {
            sum -= data[first];
            first += 1;
        }
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day9/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let data: Vec<_> = lines.iter().map(|l| u64::from_str(&l).unwrap()).collect();

    let breaking_number = find_breaking_number(&data);
    println!("{}", breaking_number);

    println!("{}", find_contiguous_sum(&data, breaking_number));
}
