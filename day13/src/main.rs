use std::{fs::File, io::{BufRead, BufReader}};
use std::str::FromStr;

fn mod_inverse(a: u64, m: u64) -> u64 {
    assert!(m != 1);

    let x = {
        let mut a = a as i64;
        let mut x = 1;
        let mut y = 0;
        let mut m = m as i64;

        while a > 1 {
            let q = a / m;
            let r = a % m;
            a = m;

            let t = y;
            y = x - q * y;
            x = t;

            m = r;
        }

        x
    };

    if x < 0 { (x + m as i64) as u64 } else { x as u64 }
}

fn get_wait_time(min_time: u64, bus_id: u64) -> u64 {
    (bus_id - min_time % bus_id) % bus_id
}

fn get_next_aligned_offset(min_time: u64, period: u64, next_offset: u64, next_bus_id: u64) -> u64 {
    let current_offset = get_wait_time(min_time, next_bus_id);
    let to_offset = (next_bus_id + current_offset - next_offset % next_bus_id) % next_bus_id;
    let inverse = mod_inverse(period, next_bus_id);
    let repeat = to_offset * inverse % next_bus_id;
    let next_min_time = min_time + period * repeat;
    assert_eq!(get_wait_time(next_min_time, next_bus_id), next_offset % next_bus_id);

    next_min_time
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day13/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    let min_time = u64::from_str(&lines[0]).unwrap();

    let buses: Vec<_> = lines.iter()
        .skip(1)
        .next()
        .unwrap()
        .split(',')
        .enumerate()
        .filter(|(_, s)| !s.is_empty() && *s != "x")
        .map(|(offset, s)| (offset as u64, u64::from_str(s).unwrap()))
        .collect();

    let first_bus = buses.iter()
        .map(|(_, bus_id)| (bus_id, get_wait_time(min_time, *bus_id)))
        .min_by_key(|(_, wait_time)| *wait_time)
        .unwrap();
    println!("{}", first_bus.0 * first_bus.1);

    let min_offset_time = buses.iter()
        .fold((min_time, 1u64), |(min_time, period), next_bus_id| (get_next_aligned_offset(min_time, period, next_bus_id.0, next_bus_id.1), period * next_bus_id.1));
    println!("{}", min_offset_time.0);
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_mod_inverse() {
        use super::mod_inverse;

        assert_eq!(mod_inverse(3, 7), 5);
        assert_eq!(mod_inverse(4, 7), 2);
        assert_eq!(mod_inverse(7, 11), 8);
    }

    #[test]
    fn test_get_wait_time() {
        use super::get_wait_time;

        assert_eq!(get_wait_time(1, 1), 0);
        assert_eq!(get_wait_time(4, 5), 1);
        assert_eq!(get_wait_time(2, 5), 3);
        assert_eq!(get_wait_time(10, 7), 4);
        assert_eq!(get_wait_time(101, 11), 9);
        assert_eq!(get_wait_time(109, 11), 1);
        assert_eq!(get_wait_time(110, 11), 0);
    }

    #[test]
    fn test_get_next_aligned_offset() {
        use super::get_next_aligned_offset;

        assert_eq!(get_next_aligned_offset(4, 2, 1, 5), 4);
        assert_eq!(get_next_aligned_offset(4, 3, 1, 5), 19);
    }
}