use std::{cell::RefCell, collections::{HashMap, HashSet}, io::{BufRead, BufReader}};
use std::rc::{Rc, Weak};
use std::fs::File;
use std::str::FromStr;

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Hash, Debug)]
struct Bag {
    adjective: String,
    color: String,
}

impl Bag {
    fn new<T: Into<String>, U: Into<String>>(adjective: T, color: U) -> Self {
        Self {
            adjective: adjective.into(),
            color: color.into(),
        }
    }
}

#[derive(Eq, PartialEq, Clone, Hash, Debug)]
struct Rule {
    container: Bag,
    contents: Vec<(usize, Bag)>,
}

impl Rule {
    fn parse(phrase: &str) -> Self {
        let mut parts = phrase.split(" contain ");
        let container_part = parts.next().unwrap();
        let contents_part = parts.next().unwrap();

        let mut container_parts = container_part.split(' ');
        let a = container_parts.next().unwrap();
        let c = container_parts.next().unwrap();
        let container = Bag::new(a.to_string(),c.to_string());

        let contents = contents_part.split(", ")
            .filter(|s| !s.eq(&"no other bags."))
            .map(|part| {
                let mut it = part.split(' ');
                let count = usize::from_str(it.next().unwrap()).unwrap();
                let a = it.next().unwrap();
                let c = it.next().unwrap();
                return (count, Bag::new(a.to_string(), c.to_string()));
            }).collect();

        Self {
            container,
            contents,
        }
    }
}

struct Node<T> {
    value: T,
    leaves: Vec<(usize, Weak<RefCell<Node<T>>>)>,
}

impl<T> Node<T> {
    fn new(value: T) -> Self {
        Self {
            value,
            leaves: Vec::new(),
        }
    }
}

struct Tree<T> {
    root: Weak<RefCell<Node<T>>>,
    nodes: HashMap<T, Rc<RefCell<Node<T>>>>,
}

impl<T: std::hash::Hash + Eq + Clone> Tree<T> {
    fn new(root_value: T) -> Self {
        let cloned = root_value.clone();
        let root_node = Rc::new(RefCell::new(Node::new(root_value)));
        let root = Rc::downgrade(&root_node );
        let mut nodes = HashMap::new();
        nodes.insert(cloned, root_node);
        Self {
            root,
            nodes,
        }
    }

    fn add(&mut self, value: T, count: usize, link_to: T) {
        let link_to_node = Rc::downgrade(self.nodes.entry(link_to.clone())
            .or_insert_with(|| Rc::new(RefCell::new(Node::new(link_to)))));

        self.nodes.entry(value.clone())
            .or_insert_with(|| Rc::new(RefCell::new(Node::new(value))))
            .borrow_mut()
            .leaves.push((count, link_to_node));
    }
}

fn part1_tree_with_rule(mut tree: Tree<Bag>, rule: Rule) -> Tree<Bag> {
    //println!("{:?}", rule);
    let Rule { container, contents } = rule;

    for (_, inside_bag) in contents.into_iter() {
        tree.add(inside_bag, 1, container.clone());
    }

    tree
}

fn part2_tree_with_rule(mut tree: Tree<Bag>, rule: Rule) -> Tree<Bag> {
    println!("{:?}", rule);
    let Rule { container, contents } = rule;

    for (count, inside_bag) in contents.into_iter() {
        tree.add(container.clone(), count, inside_bag);
    }

    tree
}

fn get_all_leaves<T: std::hash::Hash + Eq + Clone>(node: &Node<T>, leaves: &mut HashSet<T>) {
    for node in node.leaves.iter() {
        let node = Weak::upgrade(&node.1).unwrap();
        leaves.insert(node.borrow_mut().value.clone());
        get_all_leaves(&node.borrow(), leaves);
    }
}

fn count_leaves<T: std::hash::Hash + Eq + Clone>(node: &Node<T>) -> usize {
    node.leaves.iter().map(|leaf| leaf.0 * (1 + count_leaves(&Weak::upgrade(&leaf.1).unwrap().borrow()))).sum()
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day7/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let my_bag: Bag = Bag::new("shiny", "gold");

    let lines = read_lines().unwrap();
    let rules: Vec<_> = lines.iter()
        .map(|line| Rule::parse(line.as_str())).collect();
    let tree = rules.iter()
        .fold(Tree::new(my_bag.clone()), |tree, rule| part1_tree_with_rule(tree, rule.clone()));

    let mut leaves = HashSet::new();
    get_all_leaves(&Weak::upgrade(&tree.root).unwrap().borrow(), &mut leaves);
    
    //let colors: HashSet<_> = leaves.iter().map(|bag| &bag.color).collect();
    println!("{}", leaves.len());

    let tree = rules.iter()
        .fold(Tree::new(my_bag.clone()), |tree, rule| part2_tree_with_rule(tree, rule.clone()));

    println!("{}", count_leaves(&tree.root.upgrade().unwrap().borrow()));
}
