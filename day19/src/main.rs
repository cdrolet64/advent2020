use std::{fs::File, collections::HashMap, io::{BufRead, BufReader}, str::FromStr};

enum Rule {
    Litteral(char),
    Construction(Vec<Vec<usize>>),
}

impl Rule {
    fn from(s: &str) -> Self {
        match s.chars().next().unwrap() {
            '"' => Self::Litteral(s.chars().skip(1).next().unwrap()),
            _ => Self::Construction(s.split(" | ")
                .map(|construction| construction.split(' ')
                    .map(|rule_id| usize::from_str(rule_id).unwrap())
                    .collect())
                .collect()),
        }
    }
}

struct RulesSet {
    rules: HashMap<usize, Rule>,
}

impl RulesSet {
    fn from(lines: &[String]) -> Self {
        Self {
            rules: lines.iter().map(|line| {
                let mut parts = line.split(": ");
                let id = usize::from_str(parts.next().unwrap()).unwrap();
                (id, Rule::from(parts.next().unwrap()))
            }).collect(),
        }
    }

    fn set_rule(&mut self, rule_id: usize, rule: Rule) {
        self.rules.insert(rule_id, rule);
    }

    fn match_possible(&self, s: &str, rule_id: usize) -> Vec<usize> {
        match self.rules.get(&rule_id).unwrap() {
            Rule::Litteral(c) => {
                if s.chars().next() == Some(*c) {
                    vec!(1)
                } else {
                    Vec::new()
                }
            },
            Rule::Construction(constructs) => {
                constructs.iter()
                    .map(|construct| {
                        let mut matched_lengths = vec!(0);
                        for &rule_id in construct.iter() {
                            matched_lengths = matched_lengths.iter()
                                .map(|&matched_length|
                                    self.match_possible(&s[matched_length..], rule_id)
                                        .into_iter()
                                        .map(move |added_length| matched_length + added_length))
                                .flatten()
                                .collect();
                        }

                        matched_lengths
                    })
                    .flatten()
                    .collect()
            },
        }
    }

    fn matches(&self, s: &str, rule_id: usize) -> bool {
        self.match_possible(s, rule_id)
            .iter()
            .filter(|&&length| length == s.len())
            .count() > 0
    }
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day19/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();

    let mut parts = lines.split(|line| line.is_empty());
    let rules = RulesSet::from(&parts.next().unwrap());
    let messages = parts.next().unwrap();

    println!("{}", messages.iter()
        .filter(|m| rules.matches(&m, 0))
        .count());

    let mut rules = rules;
    rules.set_rule(8, Rule::Construction(vec!(vec!(42), vec!(42, 8))));
    rules.set_rule(11, Rule::Construction(vec!(vec!(42, 31), vec!(42, 11, 31))));
    println!("{}", messages.iter()
        .filter(|m| rules.matches(&m, 0))
        .count());
}

#[cfg(test)]
mod tests {
    use super::*;

    const SIMPLE_SOURCE: &str = r#"42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1"#;

    #[test]
    fn test_match() {
        let mut rules = RulesSet::from(&SIMPLE_SOURCE.split('\n').map(|s| String::from(s)).collect::<Vec<_>>());

        assert!(rules.matches("bbabbbbaabaabba", 0));
        assert!(rules.matches("ababaaaaaabaaab", 0));
        assert!(rules.matches("ababaaaaabbbaba", 0));

        assert!(!rules.matches("babbbbaabbbbbabbbbbbaabaaabaaa", 0));
        assert!(!rules.matches("aaabbbbbbaaaabaababaabababbabaaabbababababaaa", 0));
        assert!(!rules.matches("bbbbbbbaaaabbbbaaabbabaaa", 0));
        assert!(!rules.matches("bbbababbbbaaaaaaaabbababaaababaabab", 0));
        assert!(!rules.matches("baabbaaaabbaaaababbaababb", 0));
        assert!(!rules.matches("abbbbabbbbaaaababbbbbbaaaababb", 0));
        assert!(!rules.matches("aaaaabbaabaaaaababaa", 0));
        assert!(!rules.matches("aaaabbaabbaaaaaaabbbabbbaaabbaabaaa", 0));
        assert!(!rules.matches("aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba", 0));
        assert!(!rules.matches("abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa", 0));
        assert!(!rules.matches("aaaabbaaaabbaaa", 0));
        assert!(!rules.matches("babaaabbbaaabaababbaabababaaab", 0));

        rules.set_rule(8, Rule::Construction(vec!(vec!(42), vec!(42, 8))));
        rules.set_rule(11, Rule::Construction(vec!(vec!(42, 31), vec!(42, 11, 31))));

        assert!(rules.matches("bbabbbbaabaabba", 0));
        assert!(rules.matches("babbbbaabbbbbabbbbbbaabaaabaaa", 0));
        assert!(rules.matches("aaabbbbbbaaaabaababaabababbabaaabbababababaaa", 0));
        assert!(rules.matches("bbbbbbbaaaabbbbaaabbabaaa", 0));
        assert!(rules.matches("bbbababbbbaaaaaaaabbababaaababaabab", 0));
        assert!(rules.matches("ababaaaaaabaaab", 0));
        assert!(rules.matches("ababaaaaabbbaba", 0));
        assert!(rules.matches("baabbaaaabbaaaababbaababb", 0));
        assert!(rules.matches("abbbbabbbbaaaababbbbbbaaaababb", 0));
        assert!(rules.matches("aaaaabbaabaaaaababaa", 0));
        assert!(rules.matches("aaaabbaabbaaaaaaabbbabbbaaabbaabaaa", 0));
        assert!(rules.matches("aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba", 0));

        assert!(!rules.matches("abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa", 0));
        assert!(!rules.matches("aaaabbaaaabbaaa", 0));
        assert!(!rules.matches("babaaabbbaaabaababbaabababaaab", 0));
    }
}
