use std::{fs::File, io::{BufRead, BufReader}, str::FromStr};

type Rule = (String, Vec<(usize, usize)>);
type Rules = Vec<Rule>;

struct Ticket {
    numbers: Vec<usize>,
}

impl FromStr for Ticket {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            numbers: s.split(",")
                .map(|s| usize::from_str(s).unwrap())
                .collect(),
        })
    }
}

impl Ticket {
    fn is_valid(&self, rules: &[Rule]) -> bool {
        self.invalid_sum(rules) == 0
    }

    fn invalid_sum(&self, rules: &[Rule]) -> usize {
        self.numbers.iter()
            .filter(|&n|
                rules.iter()
                    .map(|rule| rule.1.iter())
                    .flatten()
                    .find(|(min, max)| n >= min && n <= max)
                    .is_none())
            .sum()
    }

    fn fits_rule_at(&self, rule: &Rule, index: usize) -> bool {
        let n = self.numbers.get(index).unwrap();
        rule.1.iter()
            .find(|(min, max)| n >= min && n <= max)
            .is_some()
    }

    fn multiply_indices(&self, indices: &[usize]) -> usize {
        indices.iter()
            .map(|i| self.numbers.get(*i).unwrap())
            .fold(1, |product, value| product * value)
    }
}

fn map_rules(tickets: &[&Ticket], rules: &[Rule]) -> Vec<usize> {
    let mut candidates = (0..).zip(rules.iter()
            .map(|rule|
                (0..tickets.first().unwrap().numbers.len()).into_iter()
                    .filter(|index| tickets.iter()
                        .all(|ticket| ticket.fits_rule_at(rule, *index)))
                    .collect::<Vec<_>>()))
        .collect::<Vec<_>>();

    candidates.sort_by_key(|c| c.1.len());
    for i in 0..candidates.len() {
        if candidates[i].1.len() == 0 {
            continue;
        }
        assert!(candidates[i].1.len() == 1);
        let index = candidates[i].1[0];
        candidates.iter_mut()
            .skip(i + 1)
            .for_each(|(_, indices)| *indices = indices.iter()
                .cloned()
                .filter(|&i2| i2 != index)
                .collect::<Vec<_>>())
    }

    candidates.sort_by_key(|c| c.0);
    candidates.iter()
        .map(|indices| indices.1.first().cloned().unwrap_or(0))
        .collect()
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day16/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let mut parts = lines.split(|s| s.is_empty());

    let rules = parts.next().unwrap();
    let rules: Rules = rules.iter()
        .map(|line| {
            let mut parts = line.split(": ");
            (String::from(parts.next().unwrap()),
            parts.next().unwrap().split(" or ").map(|range| {
                let mut parts = range.split("-");
                (usize::from_str(parts.next().unwrap()).unwrap(), usize::from_str(parts.next().unwrap()).unwrap())
            }).collect())
        })
        .collect();

    let my_ticket = Ticket::from_str(
        &parts.next()
            .unwrap()
            .iter()
            .skip(1)
            .next()
            .unwrap())
        .unwrap();

    let nearby_tickets = parts.next().unwrap();
    let nearby_tickets: Vec<Ticket> = nearby_tickets.iter()
        .skip(1)
        .map(|line| Ticket::from_str(&line).unwrap())
        .collect();

    println!("{}", nearby_tickets.iter()
        .map(|ticket| ticket.invalid_sum(&rules))
        .sum::<usize>());

    let valid_tickets: Vec<&Ticket> = nearby_tickets.iter()
        .filter(|&ticket| ticket.is_valid(&rules))
        .collect();

    let indices = map_rules(&valid_tickets, &rules);
    println!("{}", my_ticket.multiply_indices(&indices[0..6]));
}
