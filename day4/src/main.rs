use std::io::{BufRead, BufReader};
use std::fs::File;
use std::str::FromStr;
use std::collections::HashMap;

struct Passport {
    fields: HashMap<String, String>,
}

impl Passport {
    fn new() -> Self {
        Self {
            fields: HashMap::new(),
        }
    }

    fn add<T: std::iter::Iterator<Item = (String, String)>>(&mut self, entries: &mut T) {
        for (key, value) in entries {
            self.fields.insert(key, value);
        }
    }
    
    fn is_valid(&self) -> bool {
        const REQUIRED_FIELDS: &[&str] = &["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
        for req_field in REQUIRED_FIELDS {
            if !self.fields.contains_key(&req_field.to_string()) {
                return false;
            }
        }

        true
    }

    fn is_valid_part2(&self) -> bool {
        if !self.is_valid() {
            return false;
        }

        let byr = self.fields.get("byr").unwrap();
        match u32::from_str(byr) {
            Err(_) => return false,
            Ok(year) => if year < 1920 || year > 2002 { return false; },
        }

        let iyr = self.fields.get("iyr").unwrap();
        match u32::from_str(iyr) {
            Err(_) => return false,
            Ok(year) => if year < 2010 || year > 2020 { return false; },
        }

        let eyr = self.fields.get("eyr").unwrap();
        match u32::from_str(eyr) {
            Err(_) => return false,
            Ok(year) => if year < 2020 || year > 2030 { return false; },
        }

        let hgt = self.fields.get("hgt").unwrap();
        if hgt.len() < 4 {
            return false;
        }
        let (value, unit) = hgt.split_at(hgt.len() - 2);
        let value = match u32::from_str(value) {
            Err(_) => return false,
            Ok(v) => v,
        };
        match unit {
            "cm" => if value < 150 || value > 193 { return false; },
            "in" => if value < 59 || value > 76 { return false; },
            _ => return false,
        }

        let hcl = self.fields.get("hcl").unwrap();
        if hcl.len() != 7 || hcl.chars().nth(0).unwrap() != '#' {
            return false;
        }
        for c in hcl.chars().skip(1) {
            if (c < '0' || c > '9') && (c < 'a' || c > 'f') {
                return false;
            }
        }

        let ecl = self.fields.get("ecl").unwrap();
        match ecl.as_str() {
            "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => (),
            _ => return false,
        }

        let pid = self.fields.get("pid").unwrap();
        if pid.len() != 9 {
            return false;
        }
        match u32::from_str(pid) {
            Err(_) => return false,
            Ok(_) => (),
        }

        true
    }
}

fn parse_file() -> std::io::Result<Vec<Passport>> {
    let file = File::open("day4/input.txt")?;
    let reader = BufReader::new(file);

    let mut passports = Vec::new();
    let mut current = Passport::new();
    let mut has_data = false;

    for line in reader.lines() {
        let line = line?;

        if line.is_empty() {
            passports.push(current);
            current = Passport::new();
            has_data = false;
            continue;
        }

        has_data = true;
        let fields = line.split(' ');
        current.add(&mut fields.map(|pair| {
            let mut key_value_it = pair.split(':');
            let key = key_value_it.next().unwrap().to_string();
            let value = key_value_it.next().unwrap().to_string();
            (key, value)
        }));
    }

    if has_data {
        passports.push(current);
    }

    Ok(passports)
}

fn main() {
    let passports = parse_file().unwrap();
    println!("{}", passports.iter().filter(|p| p.is_valid()).count());
    println!("{}", passports.iter().filter(|p| p.is_valid_part2()).count());
}
