use std::{fs::File, str::FromStr, io::{BufRead, BufReader}};

struct InstructionResult {
    next_delta: i64,
    accumulator_delta: i64,
}

impl InstructionResult {
    fn new(next_delta: i64, accumulator_delta: i64) -> Self {
        Self {
            next_delta,
            accumulator_delta,
        }
    }
}

struct ExecutionContext {
    next_instruction: usize,
    accumulator: i64,
}

impl ExecutionContext {
    fn new() -> Self {
        Self {
            next_instruction: 0,
            accumulator: 0,
        }
    }

    fn apply(&self, result: &InstructionResult) -> Self {
        Self {
            next_instruction: ((self.next_instruction as i64) + result.next_delta) as usize,
            accumulator: self.accumulator + result.accumulator_delta,
        }
    }
}

#[derive(Debug, Clone)]
enum Instruction {
    Accumulate(i64),
    Jump(i64),
    NoOperation(i64),
}

impl Instruction {
    fn execute(&self) -> InstructionResult {
        let mut next_delta = 1;
        let mut accumulator_delta = 0;

        match self {
            Instruction::Accumulate(v) => {
                accumulator_delta += v;
            },
            Instruction::Jump(v) => {
                next_delta = *v;
            },
            Instruction::NoOperation(_) => {
            },
        }
        InstructionResult::new(next_delta, accumulator_delta)
    }
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(line: &str) -> Result<Self, Self::Err> {
        let mut parts = line.split(' ');
        let instruction_name = parts.next().unwrap();
        let value = i64::from_str(parts.next().unwrap()).unwrap();
        Ok(match instruction_name {
            "acc" => Instruction::Accumulate(value),
            "jmp" => Instruction::Jump(value),
            "nop" => Instruction::NoOperation(value),
            _ => unreachable!(),
        })
    }
}

trait InstructionFetcher {
    fn get(&self, index: usize) -> &Instruction;
    fn len(&self) -> usize;
}

impl InstructionFetcher for &[Instruction] {
    fn get(&self, index: usize) -> &Instruction {
        &self[index]
    }

    fn len(&self) -> usize {
        (*self).len()
    }
}

struct FixedProgram<'a, T> {
    base_fetcher: &'a T,
    fixed_index: usize,
    fixed_instruction: Instruction,
}

impl<'a, T> FixedProgram<'a, T> {
    fn new(base_fetcher: &'a T, fixed_index: usize, fixed_instruction: Instruction) -> Self {
        Self {
            base_fetcher,
            fixed_index,
            fixed_instruction,
        }
    }
}

impl<'a, T: InstructionFetcher> InstructionFetcher for FixedProgram<'a, T> {
    fn get(&self, index: usize) -> &Instruction {
        if index == self.fixed_index { &self.fixed_instruction } else { self.base_fetcher.get(index) }
    }

    fn len(&self) -> usize {
        self.base_fetcher.len()
    }
}

fn execute_one<T: InstructionFetcher>(instructions: &T, context: &ExecutionContext) -> ExecutionContext {
    let instruction = instructions.get(context.next_instruction);
    context.apply(&instruction.execute())
}

enum ProgramOutcome {
    InfiniteLoop,
    Terminated,
}

fn execute_program<T: InstructionFetcher>(instructions: &T) -> (ProgramOutcome, ExecutionContext) {
    let mut executed: Vec<bool> = std::iter::repeat(false).take(instructions.len()).collect();
    let mut context = ExecutionContext::new();

    while !executed[context.next_instruction] {
        executed[context.next_instruction] = true;
        context = execute_one(instructions, &context);

        if context.next_instruction == instructions.len() {
            return (ProgramOutcome::Terminated, context);
        }
    }

    (ProgramOutcome::InfiniteLoop, context)
}

fn fix_program(instructions: &[Instruction]) -> Option<i64> {
    (0usize..).zip(instructions.iter()).find_map(|(index, instruction)| {
        let replaced = match instruction {
            Instruction::Jump(v) => Instruction::NoOperation(*v),
            Instruction::NoOperation(v) => Instruction::Jump(*v),
            Instruction::Accumulate(_) => return None,
        };

        let fixed_instructions = FixedProgram::new(&instructions, index, replaced);
        let (outcome, context ) = execute_program(&fixed_instructions);
        match outcome {
            ProgramOutcome::InfiniteLoop => None,
            ProgramOutcome::Terminated => Some(context.accumulator),
        }
    })
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day8/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let instructions: Vec<_> = lines.iter().map(|line| Instruction::from_str(&line).unwrap()).collect();

    let (outcome, context) = execute_program(&instructions.as_slice());
    match outcome {
        ProgramOutcome::InfiniteLoop => println!("{}", context.accumulator),
        _ => unreachable!(),
    }

    println!("{}", fix_program(&instructions).unwrap());
}
