use std::str::FromStr;
use std::io::{BufRead, BufReader};
use std::fs::File;

const SUM: u32 = 2020;

fn parse_file() -> std::io::Result<Vec<u32>> {
    let file = File::open("day1/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| u32::from_str(&line).unwrap())).collect()
}

fn part1(values: &Vec<u32>) {
    for i in values {
        if let Ok(index) = values.binary_search(&(SUM - i)) {
            println!("{}", i * values[index]);
            return;
        }
    }
}

fn part2(values: &Vec<u32>) {
    for i in values {
        for j in values {
            if (SUM as i32 - (i + j) as i32) < 0 {
                continue;
            }

            if let Ok(index) = values.binary_search(&(SUM - i - j)) {
                println!("{}", i * j * values[index]);
                return;
            }
        }
    }
}

fn main() {
    let mut values = parse_file().unwrap();
    values.sort();
    part1(&values);
    part2(&values);
}
