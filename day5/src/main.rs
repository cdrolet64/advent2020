use std::{fs::File, io::{BufRead, BufReader}, str::FromStr};

#[derive(Ord, PartialOrd, Eq, PartialEq)]
struct BoardingPass {
    seat_id: u32,
}

impl BoardingPass {
    fn seat(&self) -> u32 {
        self.seat_id
    }
}

impl FromStr for BoardingPass {
    type Err = ();

    fn from_str(v: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            seat_id: u32::from_str_radix(v.chars().map(|c| match c {
                'F' => '0',
                'B' => '1',
                'L' => '0',
                'R' => '1',
                _ => unreachable!(),
            }).collect::<String>().as_str(), 2).unwrap(),
        })
    }
}

fn parse_file() -> std::io::Result<Vec<BoardingPass>> {
    let file = File::open("day5/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| BoardingPass::from_str(&line).unwrap())).collect()
}

fn main() {
    let passes = parse_file().unwrap();
    println!("{}", passes.iter().max().unwrap().seat());

    let mut passes = passes;
    passes.sort();
    let first  = passes.iter();
    let second = first.clone().skip(1);
    let found_seat = first.zip(second)
        .find(|(seat, next)| seat.seat() == next.seat() - 2)
        .unwrap();
    println!("{}", found_seat.0.seat() + 1);
}
