use std::{fs::File, io::{BufRead, BufReader}};
use std::str::FromStr;

const COMBINE_COUNT: [usize; 5] = [
    1,
    1, // 1
    2, // 11 2
    4, // 111 21 12 3
    7, // 1111 211 121 112 22 31 13
];

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day10/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn compute_diffs(joltages: &[u32], device_joltage: u32) -> Vec<u32> {
    let mut sorted: Vec<u32> = joltages.iter().cloned().collect();
    sorted.sort();

    let first = std::iter::once(&0).chain(sorted.iter());
    let second = sorted.iter().chain(std::iter::once(&device_joltage));

    first.zip(second)
        .map(|(current, next)| { next - current })
        .collect()
}

fn compute_chain(diffs: &[u32]) -> u32 {
    let (ones, threes): (Vec<u32>, Vec<u32>) = diffs.iter()
        .partition(|&&d| d == 1);

    (ones.len() * threes.len()) as u32
}

fn combine_count(diffs: &[u32]) -> usize {
    diffs.split(|&d| d == 3)
        .map(|ones| COMBINE_COUNT[ones.len()])
        .fold(1, |product, count| product * count)
}

fn main() {
    let lines = read_lines().unwrap();
    let joltages: Vec<_> = lines.iter().map(|l| u32::from_str(l).unwrap()).collect();

    let device_joltage = joltages.iter().max().unwrap() + 3u32;
    let diffs = compute_diffs(&joltages, device_joltage);

    println!("{}", compute_chain(&diffs));
    println!("{}", combine_count(&diffs));
}
