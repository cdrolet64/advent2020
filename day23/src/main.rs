#[derive(PartialEq, Eq, Debug)]
struct CircularCups {
    cups: Vec<usize>,
}

impl CircularCups {
    fn new(cups: Vec<usize>) -> Self {
        let cups = cups.iter()
            .map(|cup| *cup - 1)
            .zip(cups.iter()
                .skip(1)
                .map(|cup| *cup - 1)
                .chain(std::iter::once(*cups.first().unwrap() - 1)))
            .fold(std::iter::repeat(0).take(cups.len()).collect::<Vec<usize>>(), |mut cups, (cup, next)| {
                cups[cup] = next;
                cups
            });
        Self {
            cups,
        }
    }

    fn from(mut value: usize) -> Self {
        let mut cups = Vec::new();
        while value > 0 {
            cups.push(value % 10);
            value /= 10;
        }
        cups.reverse();
        Self::new(cups)
    }

    fn to_value(&self) -> usize {
        self.iter(0)
            .skip(1)
            .take(self.cups.len() - 1)
            .fold(0, |value, cup| value * 10 + cup + 1)
    }

    fn len(&self) -> usize {
        self.cups.len()
    }

    fn next(&self, cup: usize) -> usize {
        self.cups[cup]
    }

    fn set_next(&mut self, cup: usize, next: usize) {
        self.cups[cup] = next;
    }

    fn iter(&self, current_cup: usize) -> CircularIterator<'_> {
        CircularIterator {
            cups: self,
            current_cup: current_cup,
        }
    }
}

#[derive(Clone)]
struct CircularIterator<'a> {
    cups: &'a CircularCups,
    current_cup: usize,
}

impl<'a> Iterator for CircularIterator<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let cup = self.current_cup;
        self.current_cup = self.cups.next(cup);
        Some(cup)
    }
}

fn compute_target_cup(current_cup: usize, cups_to_move: &Vec<usize>, cups_count: usize) -> usize {
    let mut next_cup = current_cup;
    while next_cup == current_cup || cups_to_move.iter().find(|&&cup_to_move| cup_to_move == next_cup).is_some() {
        next_cup = (next_cup + cups_count - 1) % cups_count;
    }
    
    next_cup
}

fn mix_cups(mut cups: CircularCups, mut current_cup: usize, cups_count_to_move: usize, rounds: usize) -> CircularCups {
    for _ in 0..rounds {
        let cups_to_move: Vec<usize> = cups.iter(current_cup)
            .skip(1)
            .take(cups_count_to_move)
            .collect();
        
        let target_cup = compute_target_cup(current_cup, &cups_to_move, cups.len());
        let next_of_target = cups.next(target_cup);

        let last_to_move = *cups_to_move.last().unwrap();
        let new_next_of_current = cups.next(last_to_move);

        cups.set_next(current_cup, new_next_of_current);
        cups.set_next(target_cup, *cups_to_move.first().unwrap());
        cups.set_next(last_to_move, next_of_target);

        current_cup = cups.next(current_cup);
    }

    cups
}

fn main() {
    let input = 318946572;

    let final_cups = mix_cups(CircularCups::from(input), 3 - 1, 3, 100);
    println!("{}", final_cups.to_value());

    let final_cups = mix_cups(CircularCups::new([3,1,8,9,4,6,5,7,2].iter().cloned().chain((10..=1_000_000).into_iter()).collect()), 3 - 1, 3, 10_000_000);
    println!("{:?}", &final_cups.iter(0).skip(1).take(2).map(|cup| cup + 1).product::<usize>());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mix_parse() {
        assert_eq!(CircularCups::from(389125467).to_value(), 25467389);
    }

    #[test]
    fn test_mix_cups() {
        let cups = CircularCups::from(389125467);
        let final_cups = mix_cups(cups, 3 - 1, 3, 1);
        assert_eq!(final_cups.to_value(), 54673289);

        let cups = CircularCups::from(389125467);
        let final_cups = mix_cups(cups, 3 - 1, 3, 10);
        assert_eq!(final_cups.to_value(), 92658374);
    }

    #[test]
    fn test_clockwise_from_1() {
        let cups = CircularCups::from(583741926);
        assert_eq!(cups.to_value(), 92658374);
    }
    
    #[test]
    fn test_100_moves() {
        let cups = CircularCups::from(389125467);
        let final_cups = mix_cups(cups, 3 - 1, 3, 100);
        assert_eq!(final_cups.to_value(), 67384529);
    }
}