use std::{collections::HashSet, io::{BufRead, BufReader}};
use std::fs::File;

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day6/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let groups: Vec<_> = lines.split(|s| s.is_empty()).collect();
    let count = groups.iter()
        .map(|group| group.iter()
            .map(|line| line.chars().collect::<HashSet<_>>())
            .fold(HashSet::new(), |answers, current| answers.union(&current).cloned().collect()))
        .map(|a| a.len())
        .sum::<usize>();
    println!("{}", count);
    
    let count = groups.iter()
        .map(|group| group.iter()
            .map(|line| line.chars().collect::<HashSet<_>>())
            .fold(('a'..='z').collect::<HashSet<_>>(), |answers, current| answers.intersection(&current).cloned().collect()))
        .map(|a| a.len())
        .sum::<usize>();
    println!("{}", count);
}
