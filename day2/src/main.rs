use std::str::FromStr;
use std::io::{BufRead, BufReader};
use std::fs::File;

struct PasswordInfo {
    min: usize,
    max: usize,
    letter: char,
    password: String,
}

impl PasswordInfo {
    fn is_valid(&self) -> bool {
        let count = self.password.chars().filter(|c| c == &self.letter).count();
        count >= self.min && count <= self.max
    }

    fn is_valid_part2(&self) -> bool {
        let first = self.password.chars().nth(self.min - 1).unwrap();
        let second = self.password.chars().nth(self.max - 1).unwrap();
        (first == self.letter) != (second == self.letter)
    }
}

impl FromStr for PasswordInfo {
    type Err = <usize as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(": ");
        let mut infos = parts.next().unwrap().split(" ");
        let mut minmax = infos.next().unwrap().split("-");
        Ok(Self {
            min: usize::from_str(minmax.next().unwrap())?,
            max: usize::from_str(minmax.next().unwrap())?,
            letter: infos.next().unwrap().chars().nth(0).unwrap(),
            password: parts.next().unwrap().to_string(),
        })
    }
}

fn parse_file() -> std::io::Result<Vec<PasswordInfo>> {
    let file = File::open("day2/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| PasswordInfo::from_str(&line).unwrap())).collect()
}

fn main() {
    let passwords = parse_file().unwrap();

    let count = passwords.iter().filter(|pi| pi.is_valid()).count();
    println!("{}", count);

    let count = passwords.iter().filter(|pi| pi.is_valid_part2()).count();
    println!("{}", count);
}
