use std::{fs::File, str::FromStr, io::{BufRead, BufReader}};

type Point = (i64, i64);

enum Rotation {
    Left,
    Right,
}

#[derive(Clone, Copy)]
enum Direction {
    East,
    North,
    West,
    South,
}

impl Direction {
    fn rotate(&self, r: &Rotation) -> Self {
        let quarter = match self {
            Self::East => 0,
            Self::North => 1,
            Self::West => 2,
            Self::South => 3,
        };

        let new_quarter = quarter + 4 + match r {
            Rotation::Left => 1,
            Rotation::Right => -1,
        };

        match new_quarter % 4 {
            0 => Self::East,
            1 => Self::North,
            2 => Self::West,
            3 => Self::South,
            _ => unreachable!(),
        }
    }

    fn transform(&self, p: &Point) -> Point {
        let (x, y) = p;
        match self {
            Self::East =>  (*x, *y),
            Self::North => (-y, *x),
            Self::West =>  (-x, -y),
            Self::South => (*y, -x),
        }
    }
}

struct ShipState {
    direction: Direction,
    position: Point,
}

impl ShipState {
    fn new(direction: Direction, position: Point) -> Self {
        Self {
            direction,
            position,
        }
    }
}

struct ShipWithWaypoint {
    ship: Point,
    waypoint: Point,
}

impl ShipWithWaypoint {
    fn new(ship: Point, waypoint: Point) -> Self {
        Self {
            ship,
            waypoint,
        }
    }
}

enum Action {
    Forward(u64),
    Move(Direction, u64),
    Rotate(Rotation, u64),
}

impl Action {
    fn execute(&self, ship: &ShipState) -> ShipState {
        match self {
            Self::Forward(distance) => ShipState::new(ship.direction, add(&ship.position, &ship.direction.transform(&(*distance as i64, 0)))),
            Self::Move(d, distance) => ShipState::new(ship.direction, add(&ship.position, &d.transform(&(*distance as i64, 0)))),
            Self::Rotate(r, c) => ShipState::new((0..*c).fold(ship.direction, |d, _| d.rotate(r)), ship.position),
        }
    }

    fn execute_with_waypoint(&self, sw: &ShipWithWaypoint) -> ShipWithWaypoint {
        match self {
            Self::Forward(distance) => ShipWithWaypoint::new(add(&sw.ship, &scale(&sw.waypoint, *distance as i64)), sw.waypoint.clone()),
            Self::Move(d, distance) => ShipWithWaypoint::new(sw.ship.clone(), add(&sw.waypoint, &d.transform(&(*distance as i64, 0)))),
            Self::Rotate(r, c) => ShipWithWaypoint::new(sw.ship.clone(), (0..*c).fold(Direction::East, |d, _| d.rotate(r)).transform(&sw.waypoint)),
        }
    }
}

impl FromStr for Action {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = u64::from_str(&s[1..]).unwrap();
        Ok(match s.chars().next().unwrap() {
            'N' => Self::Move(Direction::North, value),
            'S' => Self::Move(Direction::South, value),
            'E' => Self::Move(Direction::East,  value),
            'W' => Self::Move(Direction::West,  value),
            'L' => Self::Rotate(Rotation::Left, value / 90),
            'R' => Self::Rotate(Rotation::Right, value / 90),
            'F' => Self::Forward(value),
            _ => unreachable!(),
        })
    }
}

fn add(p1: &Point, p2: &Point) -> Point {
    (p1.0 + p2.0, p1.1 + p2.1)
}

fn scale(p: &Point, scaling: i64) -> Point {
    (scaling * p.0, scaling * p.1)
}

fn manhattan_distance(p: &Point) -> u64 {
    (p.0.abs() + p.1.abs()) as u64
}

fn read_lines() -> std::io::Result<Vec<String>> {
    let file = File::open("day12/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().collect()
}

fn main() {
    let lines = read_lines().unwrap();
    let actions: Vec<Action> = lines.iter()
        .map(|line| Action::from_str(&line).unwrap())
        .collect();

    let end_state = actions.iter()
        .fold(ShipState::new(Direction::East, (0, 0)), |ship_state, action| action.execute(&ship_state));
    println!("{}", manhattan_distance(&end_state.position));

    let end_state = actions.iter()
        .fold(ShipWithWaypoint::new((0, 0), (10, 1)), |sw, action| action.execute_with_waypoint(&sw));
    println!("{}", manhattan_distance(&end_state.ship));
}
