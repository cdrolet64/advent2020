use std::collections::HashMap;

fn get_n_spoken(starting: &[usize], nth: usize) -> usize {
    let mut last_diff = *starting.last().unwrap();
    let mut indices = starting.iter().enumerate().fold(
        HashMap::<usize, (usize, usize)>::with_capacity(5000),
        |mut indices, (i, n)| {
            indices.entry(*n).and_modify(|ifl| { *ifl = (i, ifl.0); }).or_insert((i, i));
            indices
        });
    
    for i in starting.len()..nth {
        let target = last_diff;
        let diff = indices.get(&target).map(|(i_last, i_first)| *i_last - *i_first).unwrap_or(0);
        indices.entry(diff).and_modify(|ifl| { *ifl = (i, ifl.0); }).or_insert((i, i));
        last_diff = diff;
    }

    last_diff
}

fn main() {
    //let lines = read_lines().unwrap();
    let numbers = vec!(16,1,0,18,12,14,19); // 0,5,0,2,0,2,2,1,

    println!("{}", get_n_spoken(&numbers, 2020));
    println!("{}", get_n_spoken(&numbers, 30000000));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_n_spoken() {
        assert_eq!(get_n_spoken(&[1,3,2], 2020), 1);
        assert_eq!(get_n_spoken(&[2,1,3], 2020), 10);
    }
}