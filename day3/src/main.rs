use std::io::{BufRead, BufReader};
use std::fs::File;

enum SquareType {
    Open,
    Tree,
}

fn read_line(line: &str) -> Vec<SquareType> {
    line.chars().map(|c| if c == '#' { SquareType::Tree } else { SquareType::Open }).collect()
}

fn parse_file() -> std::io::Result<Vec<Vec<SquareType>>> {
    let file = File::open("day3/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| read_line(&line))).collect()
}

fn count_trees(map: &Vec<Vec<SquareType>>, dx: usize, dy: usize) -> usize{
    let mut x = 0;
    let mut y = 0;
    let mut count = 0;

    while y < map.len() {
        match map[y][x] {
            SquareType::Tree => count += 1,
            SquareType::Open => (),
        }

        x = (x + dx) % map[y].len();
        y += dy;
    }

    count
}

fn main() {
    let map = parse_file().unwrap();
    println!("{}", count_trees(&map, 3, 1));
    println!("{}", count_trees(&map, 1, 1) * count_trees(&map, 3, 1) * count_trees(&map, 5, 1) * count_trees(&map, 7, 1) * count_trees(&map, 1, 2));
}
